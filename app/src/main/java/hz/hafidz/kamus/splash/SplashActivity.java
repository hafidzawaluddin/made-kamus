package hz.hafidz.kamus.splash;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import hz.hafidz.kamus.R;
import hz.hafidz.kamus.helper.ContentHelper;
import hz.hafidz.kamus.helper.ContentModel;
import hz.hafidz.kamus.helper.SharedPrefHelper;
import hz.hafidz.kamus.main.MainActivity;

 public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LoadData().execute();
    }

    private class LoadData extends AsyncTask<Void, Integer, Void> {
        ContentHelper helper;
        SharedPrefHelper sharedPrefHelper;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            helper = new ContentHelper(SplashActivity.this).open();
            sharedPrefHelper = new SharedPrefHelper(SplashActivity.this);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ArrayList<ContentModel> englishModels = getEnglishData();
            ArrayList<ContentModel> indonesianModels = getIndonesianData();
            Boolean isFirst = sharedPrefHelper.getIsNew();


            if (isFirst) {
                helper.beginTransaction();
                Log.wtf("doInBackground: ", "pertama");
                try {
                    for (ContentModel model : englishModels) {
                        helper.insertEnglish(model);
                    }
                    for (ContentModel model : indonesianModels) {
                        helper.insertIndonesian(model);
                    }
                    helper.setTransactionSuccess();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                helper.endTransaction();
                sharedPrefHelper.setIsNew(false);
                helper.close();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    public ArrayList<ContentModel> getEnglishData() {
        ArrayList<ContentModel> models = new ArrayList<>();
        String line;
        BufferedReader bufferedReader;
        try {

            Resources resources = getResources();
            InputStream raw = resources.openRawResource(R.raw.english_indonesia);
            bufferedReader = new BufferedReader(new InputStreamReader(raw));
            int count = 1;
            do {
                line = bufferedReader.readLine();
                if (line != null) {
                    ContentModel model = new ContentModel(count, line);
                    models.add(model);
                    count++;
                }
            } while (line != null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return models;
    }

    public ArrayList<ContentModel> getIndonesianData() {
        ArrayList<ContentModel> models = new ArrayList<>();
        String line;
        BufferedReader bufferedReader;
        try {

            Resources resources = getResources();
            InputStream raw = resources.openRawResource(R.raw.indonesia_english);
            bufferedReader = new BufferedReader(new InputStreamReader(raw));
            int count = 1;
            do {
                line = bufferedReader.readLine();
                if (line != null) {
                    ContentModel model = new ContentModel(count, line);
                    models.add(model);
                    count++;
                }
            } while (line != null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return models;
    }
}
