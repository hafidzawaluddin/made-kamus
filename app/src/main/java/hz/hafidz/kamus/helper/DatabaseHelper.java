package hz.hafidz.kamus.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "kamus";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE_ENGLISH_INDONESIA = "CREATE TABLE " +
            DatabaseContract.English_Indonesia + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DatabaseContract.CONTENT + " TEXT NOT NULL)";

    private static final String CREATE_TABLE_INDONESIA_ENGLISH = "CREATE TABLE " +
            DatabaseContract.Indonesia_English + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DatabaseContract.CONTENT+ " TEXT NOT NULL)";

    private static final String DELETE_TABLE_ENGLISH_INDONESIA = "DROP TABLE IF EXISTS "
            + DatabaseContract.English_Indonesia;

    private static final String DELETE_TABLE_INDONESIA_ENGLISH = "DROP TABLE IF EXISTS "
            + DatabaseContract.Indonesia_English;

    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        System.out.println(CREATE_TABLE_ENGLISH_INDONESIA);
        System.out.println(CREATE_TABLE_INDONESIA_ENGLISH);
        sqLiteDatabase.execSQL(CREATE_TABLE_ENGLISH_INDONESIA);
        sqLiteDatabase.execSQL(CREATE_TABLE_INDONESIA_ENGLISH);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DELETE_TABLE_ENGLISH_INDONESIA);
        sqLiteDatabase.execSQL(DELETE_TABLE_INDONESIA_ENGLISH);
        onCreate(sqLiteDatabase);
    }
}
