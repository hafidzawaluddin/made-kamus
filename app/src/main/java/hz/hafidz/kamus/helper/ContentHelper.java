package hz.hafidz.kamus.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class ContentHelper {
    private Context context;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    public ContentHelper(Context context) {
        this.context = context;
    }

    public ContentHelper open() throws SQLException {
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        databaseHelper.close();
    }

    public void insertEnglish(ContentModel model) {
        ContentValues initialValues = new ContentValues();
//        System.out.println(model.getContent());
        initialValues.put(DatabaseContract.CONTENT, model.getContent());
        database.insert(DatabaseContract.English_Indonesia, null, initialValues);
    }

    public void insertIndonesian(ContentModel model) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(DatabaseContract.CONTENT, model.getContent());
        database.insert(DatabaseContract.Indonesia_English, null, initialValues);
    }

//    public ArrayList<ContentModel> getEnglishIndonesia(String name){
//        String result = "";
//        Cursor cursor = database.query(DatabaseContract.English_Indonesia, null,
//                name + "LIKE ?", new String[]{name}, null, null, "id ASC", null);
//        cursor.moveToFirst();
//        ArrayList<ContentModel> models = new ArrayList<>();
//        ContentModel model;
//        if (cursor.getCount() > 0 ){
//            do {
//                model = new ContentModel();
//                model.setId(cursor.getInt(cursor.getColumnIndexOrThrow("id")));
//                model.setContent(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CONTENT)));
//                models.add(model);
//                cursor.moveToNext();
//            } while (!cursor.isAfterLast());
//        }
//        cursor.close();
//        return models;
//    }
//
//    public ArrayList<ContentModel> getIndonesiaEnglish(String name){
//        String result = "";
//        Cursor cursor = database.query(DatabaseContract.Indonesia_English, null,
//                name + "LIKE ?", new String[]{name}, null, null, "id ASC", null);
//        cursor.moveToFirst();
//        ArrayList<ContentModel> models = new ArrayList<>();
//        ContentModel model;
//        if (cursor.getCount() > 0 ){
//            do {
//                model = new ContentModel();
//                model.setId(cursor.getInt(cursor.getColumnIndexOrThrow("id")));
//                model.setContent(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CONTENT)));
//                models.add(model);
//                cursor.moveToNext();
//            } while (!cursor.isAfterLast());
//        }
//        cursor.close();
//        return models;
//    }

    public List<ContentModel> getIndonesian() {
        String sql = "SELECT * FROM " + DatabaseContract.Indonesia_English;
        Cursor c = database.rawQuery(sql, null);
        List<ContentModel> model=  new ArrayList<>();
        c.moveToFirst();
        do{
            model.add(new ContentModel(c.getInt(0),c.getString(1)));
        } while (c.moveToNext());
        c.close();
        System.out.println(model.size());
        return model;
    }

    public List<ContentModel> getEnglish() {
        Cursor c = database.rawQuery("SELECT * FROM " + DatabaseContract.English_Indonesia, null);
        List<ContentModel> model=  new ArrayList<>();
        c.moveToFirst();
        do{
            model.add(new ContentModel(c.getInt(0),c.getString(1)));
        } while (c.moveToNext());
        c.close();
        return model;
    }

    public void beginTransaction() {
        database.beginTransaction();
    }

    public void setTransactionSuccess() {
        database.setTransactionSuccessful();
    }

    public void endTransaction() {
        database.endTransaction();
    }
}
