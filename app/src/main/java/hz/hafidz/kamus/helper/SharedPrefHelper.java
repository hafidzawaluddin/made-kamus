package hz.hafidz.kamus.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefHelper {
    public static String IS_NEW = "isNew";
    private SharedPreferences sharedPreferences;

    public SharedPrefHelper(Context context) {
        String prefName = "kamusPref";
        sharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

    public void setIsNew(Boolean isNew) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_NEW, isNew);
        editor.apply();
    }

    public Boolean getIsNew() {
        return sharedPreferences.getBoolean(IS_NEW, true);
    }
}
