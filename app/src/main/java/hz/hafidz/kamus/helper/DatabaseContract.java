package hz.hafidz.kamus.helper;

import android.provider.BaseColumns;

public class DatabaseContract {

    public static String English_Indonesia = "englishindonesia";
    public static String Indonesia_English = "indonesiaenglish";
    public static String CONTENT = "content";

    public static final class EnglishIndonesiaColumns implements BaseColumns{
        static String content = "content";
    }

    public static final class IndonesiaEnglish implements BaseColumns{
        static String content = "content";
    }
}
