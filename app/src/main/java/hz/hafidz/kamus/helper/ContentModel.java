package hz.hafidz.kamus.helper;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class ContentModel implements Parcelable {
    int id;
    String content;

    public ContentModel(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public ContentModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.content);
    }

    protected ContentModel(Parcel in) {
        this.id = in.readInt();
        this.content = in.readString();
    }

    public static final Parcelable.Creator<ContentModel> CREATOR = new Parcelable.Creator<ContentModel>() {
        @Override
        public ContentModel createFromParcel(Parcel source) {
            return new ContentModel(source);
        }

        @Override
        public ContentModel[] newArray(int size) {
            return new ContentModel[size];
        }
    };
}
