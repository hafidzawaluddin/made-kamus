package hz.hafidz.kamus.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import hz.hafidz.kamus.R;
import hz.hafidz.kamus.helper.ContentModel;

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>{

    private List<ContentModel> mModel;

     MainRecyclerAdapter(List<ContentModel> mModel) {
        this.mModel = mModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ContentModel model = mModel.get(i);
        viewHolder.txtItem.setText(model.getContent());
    }

    @Override
    public int getItemCount() {
        if(mModel != null){
            return mModel.size();
        } else {
            return 0;
        }
    }

    void cleanData(List<ContentModel> list){
        mModel.clear();
        mModel.addAll(list);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView txtItem;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItem = itemView.findViewById(R.id.txtList);
        }
    }
}
