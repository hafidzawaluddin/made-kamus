package hz.hafidz.kamus.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hz.hafidz.kamus.R;
import hz.hafidz.kamus.helper.ContentHelper;
import hz.hafidz.kamus.helper.ContentModel;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private MainRecyclerAdapter mAdapter;
    private ArrayList<ContentModel> indonesian;
    private ArrayList<ContentModel> english;
    private Boolean isSwap = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setData();
        onClick();
        setRecycler();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onClick() {
        EditText txtSearch = findViewById(R.id.edtQuery);
        TextView form = findViewById(R.id.txtForm);
        TextView to = findViewById(R.id.txtTo);
        ImageView btnSwap = findViewById(R.id.btnSwap);


        btnSwap.setOnClickListener(view -> {
            if (isSwap) {
                form.setText(getText(R.string.english));
                to.setText(getText(R.string.indonesia));
                isSwap = false;
            } else {
                form.setText(getText(R.string.indonesia));
                to.setText(getText(R.string.english));
                isSwap = true;
            }
        });


        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (isSwap) {
                    mAdapter.cleanData(getQuery(indonesian, txtSearch.getText().toString()));
                } else {
                    mAdapter.cleanData(getQuery(english, txtSearch.getText().toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private List<ContentModel> getQuery(List<ContentModel> list, String query) {
        List<ContentModel> filter = new ArrayList<>();
        for (ContentModel model : list) {
            if (model.getContent().startsWith(query)) {
                filter.add(model);
            }
        }
        return filter;
    }

    private void setRecycler() {
        List<ContentModel> list = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.recyclerMain);
        mAdapter = new MainRecyclerAdapter(list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
    }

    private void setData() {
        ContentHelper mHelper = new ContentHelper(this);
        mHelper.open();
        indonesian = new ArrayList<>();
        english = new ArrayList<>();
        indonesian.addAll(mHelper.getIndonesian());
        english.addAll(mHelper.getEnglish());

    }

}
